#!/bin/bash

mobLog="/var/log/Mobyle/access_log"
rpbsLogP="/nfs/eurydice/aw3/RPBS_LIB/logs"

# Country report ?
if [ -n $1 ] && [ $1 = "1" ]
then
    cnt=1
else
    cnt=0
fi

cmd="./AccessLogInfos.py -I 192.168.*.*::172.27.6.* -D 10_01_01:: "

for i in "-S fpocket2 $mobLog -O $rpbsLogP/fpocket" "-S hpocket $mobLog -O $rpbsLogP/hpocket" "-S mdpocket $mobLog -O $rpbsLogP/mdpocket"
do

    cmd=$cmd" $i"
    if [ $cnt -eq 1 ]
    then
	cmd=$cmd" -c"
    fi
    echo -e "\n# -----------------------------------------------"
    echo "# mobyle/offmobyle: $i"
    echo $cmd | sh
    
done
