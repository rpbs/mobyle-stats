#!/bin/bash

mobLog="/var/log/Mobyle/access_log"
rpbsLogP="/nfs/eurydice/aw3/RPBS_LIB/logs"
sF="services.txt"

# Country report ?
if [ -n $1 ] && [ $1 = "1" ]
then
    cnt=1
else
    cnt=0
fi

grep -v \# $sF | while read line
do   
    mob=`echo $line | awk -F ";" '{print $1}' | sed 's/ //g'`
    omob=`echo $line | awk -F ";" '{print $2}' | sed 's/ //g'`

    cmd="./AccessLogInfos.py -I 192.168.*.*::172.27.6.* -D 10_01_01::11_01_01 "
    #cmd="./AccessLogInfos.py -I 192.168.*.*::172.27.6.* -D 10_01_01:: "
    # Mobyle
    if [ $mob != "None" ]
    then
	cmd=$cmd" -S $mob $mobLog"
    fi
    # Off Mobyle
    if [ $omob != "None" ]
    then
	cmd=$cmd" -O $rpbsLogP/$omob"
    fi
    # Country report ?
    if [ $cnt -eq 1 ]
    then
	cmd=$cmd" -c"
    fi
    
    echo -e "\n# -----------------------------------------------"
    echo "# mobyle/offmobyle: $line"
    #echo "# $cmd"
    echo $cmd | sh
    
done
