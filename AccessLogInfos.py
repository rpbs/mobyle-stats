#!/usr/bin/env python
# -*- coding: utf-8 -*-

# -- IMPORTS --

# Python standards modules
import sys
import os
import os.path
import shutil
import subprocess
import time
import datetime
import socket
from optparse import OptionParser
from operator import itemgetter


# -- GLOBALS --
VERSION = "0.3"

# -- HISTORY --
# -- 2009, fev 03
# Fist implementation
#
# -- 2009, nov 12
# * Add -c option:  per country usage report
# * Add -m option: per month usage report


# -- CONFIGURATION --
"""
AccessLogInfos.py

@author: Julien Maupetit

@organisation : Ressource Parisienne en Bioinformatique Structurale
(RPBS) - Universite Paris Diderot Paris 7 - MTi INSERM U973

@contact: julien.maupetit@univ-paris-diderot.fr

@version: %s
""" % VERSION

# -- ARGUMENTS --
def cmdLine():
    """
    Command line definition.

    @return: command line options and arguments (optionParser object)
    """
    
    vStr = "%prog"+ " v%s"%VERSION
    cmd = OptionParser("usage: %prog [options] <LOG file> ", version = vStr)

    cmd.add_option("-S", action="store",      dest="service", type="string",
                   help="specific service to analyse",
                   default = None)    

    cmd.add_option("-D", action="store",      dest="dateLim", type="string",
                   help="date limits analyse (format: YY_MM_DD::YY_MM_DD)",
                   default = None)

    cmd.add_option("-I", action="store",      dest="blacklist",  type="string",
                   help="ignore IP matching the mask (format: 192.168.*.*::172.27.6.*)",
                   default = None)

    cmd.add_option("-i", action="store_true", dest="ipv",        
                   help="print unique IPs for each service",
                   default = False)

    cmd.add_option("-c", action="store_true", dest="cUsage",        
                   help="per country usage report",
                   default = False)

    cmd.add_option("-m", action="store_true", dest="mUsage",        
                   help="per month usage report",
                   default = False)

    cmd.add_option("-O", action="store",      dest="offMobLog",  type="string",
                   help="off mobyle log file",
                   default = False)
    
    cmd.add_option("-v", action="store_true", dest="verbose",   
                   help="verbose mode",
                   default = False)

    cmd.add_option("-d", action="store_true", dest="debug",     
                   help="debugging mode",
                   default = False)

    return cmd

def cmdResume( options, args ):
    """
    Print a resume of the user input command
    """
    
    print "# --"
    print "# AccessLogInfos v%s" % VERSION
    print "# --"
    print "# Options:"
    print "#", options
    print "# Arguments:"
    print "#", args
    print "#"
    
    return

def argParse( resume = 0 ):
    """
    Check and parse arguments.
    """

    pCmd = cmdLine()
    try:
        (options, args) = pCmd.parse_args()
    except ValueError:
        pCmd.error("\nInvalid command line format !\n")

    return (options, args)


# -- IO MANAGEMENT --
def openFile( FN, mode = "w", verbose = 0 ):
    """
    Clean open a file. Returns a file handle (stream).

    @param FN     : input file name
    @param mode   : openning mode r,w,a ...
    @param verbose: verbose mode
    """
    try:
        iF = open( FN, mode )
    except:
        print >> sys.stderr, "Error: impossible to open %s file in '%s' mode !" % ( FN, mode )
        sys.exit(0)
        
    return iF


def closeFile( iF, verbose = 0 ):
    """
    Clean closing of a file.
    
    @param iF     : file handle to close
    @param verbose: verbose mode
    """
    
    try:
        iF.close()
    except:
        print >> sys.stderr, "Error: Could not close file handle :", iF
        sys.exit(0)

    return


def fileGetLines( FN, ret = 0, verbose = 0 ):
    """
    Open txt file, read and returns lines.

    @param FN     : input file name
    @param ret    : do we need to ignore carriage return
    @param verbose: verbose mode
    """

    if verbose:
        print >> sys.stderr, "Will read %s file lines ... "%FN

    lines = []
    
    F = openFile( FN, mode = "r", verbose = verbose )
        
    try:
        lines = F.readlines()
    except:
        print >> sys.stderr, "Error: Could not parse file %s !"%FN
        sys.exit(0)

    oL = []
    for aL in lines:
        if aL.strip() == "\n":
            continue
        if ret == 0:
            oL.append(aL.replace("\n",""))
        else:
            oL.append(aL)

    closeFile( F, verbose = verbose )

    return oL

def writeList( handle, lines, ret = 0, verbose = 0 ):
    """
    Write a string (list) to an open file handle

    @param handle : the file handle
    @param lines  : line(s) (list) to write
    @param ret    : do we need to add carriage return
    @param verbose: verbose mode
    """

    if isinstance( lines, str ):
        if ret:
            aStr = '%s\n' % lines
        else:
            aStr = '%s' % lines
        try:
            handle.write( aStr )
        except:
            print >> sys.stderr, "Error: impossible to write %s line to file. Aborted." % lines
            sys.exit(0)
    elif isinstance( lines, list ):
        for line in lines:
            if ret:
                aStr = '%s\n' % line
            else:
                aStr = '%s' % line
            try:
                handle.write( aStr )
            except:
                print >> sys.stderr, "Error: impossible to write %s line to file. Aborted." % line
                sys.exit(0)
    return
    
def filePutLines( FN, lines, ret = 0, verbose = 0 ):
    """
    Flat lines into a file.
    
    @param FN     : output file name
    @param lines  : lines to write into FN
    @param ret    : do we need to add carriage return
    @param verbose: verbose mode
    """
    
    if verbose:
        print >> sys.stderr, "Will write lines to %s file ... " % FN

    F = openFile( FN, mode = "w", verbose = verbose )
    writeList( F, lines, ret = ret, verbose = verbose )
    closeFile( F, verbose = verbose )

    return 

def checkFileStatus( FN, verbose = 0 ):
    """
    Check if the input file exists and is not empty.

    @param FN     : input file name
    @param verbose: verbose mode
    
    @return: file status (0 if ok)    
    """

    if not os.path.exists( FN ):
        return 1

    statinfo = os.stat( FN )
    
    if statinfo.st_size == 0:
        return 2

    return 0


def shellExec( cmd, stdinl = [], ignerr = False, verbose = 0 ):
    """
    Execute a shell command, with pipelines to stdin, stdout, and stderr.
    
    @param cmd    : the command line
    @param stdinl : string list to pipe as input
    @param ignerr : ignore stderr process content
    @param verbose: verbose mode

    @return: a tuple as (stdout lines list,stderr lines list)
    """

    if verbose:
        print >> sys.stderr, "ShellExec command: %s" % cmd

    if len( stdinl ):
        p = subprocess.Popen(cmd, 
                             shell=True, 
                             stdin=subprocess.PIPE, 
                             stdout=subprocess.PIPE, 
                             stderr=subprocess.PIPE, 
                             close_fds=True)
        # Prepare stdin
        iStr = ""
        for aIn in stdinl:
            iStr += "%s\n" % aIn
        # Write to stdin
        p.stdin.write( "%s" % iStr )
        # Close pipe
        p.stdin.close()
    else:
        p = subprocess.Popen(cmd, 
                             shell=True, 
                             stdout=subprocess.PIPE, 
                             stderr=subprocess.PIPE, 
                             close_fds=True)
    
    # Read stdout/err
    stdoutl = []
    stderrl = []
    while True:
        aStdout = p.stdout.readline()
        aStderr = p.stderr.readline()
        if verbose >1:
            print >> sys.stderr, aStdout,
        if verbose >1:
            print >> sys.stderr, aStderr,
        if aStdout:
            stdoutl.append( aStdout )
        if aStderr:
            stderrl.append( aStderr )
        if not aStdout and not aStderr:
            break

    if len(stderrl) and not ignerr:
        writeList( sys.stderr, stderrl )

    if verbose:
        print >> sys.stderr, ""

    return (stdoutl,stderrl)

def shellScript( cmd, outFile = None, shell = "/bin/bash", hdr = None, verbose = 0 ):
    """
    Write (a) shell command(s) in a shell script.
    
    @param cmd    : the command line (list)
    @param outFile: string list to pipe as input
    @param shell  : path to your favourite shell interpreter
    @param hdr    : script header line (or list) (usefull for PBS script)
    @param verbose: verbose mode
    """

    if verbose:
        print >> sys.stderr, "ShellScript output file: %s" % outFile
        print >> sys.stderr, "ShellScript shell is : %s" % shell
        print >> sys.stderr, "ShellScript command(s): %s" % str(cmd)
    
    oLines = []
    oLines.append( "#!%s" % shell )
    
    # -- Script header
    # if hdr is a list, let's unlist it !
    if hdr:
        if isinstance( hdr, str ):
            oLines.append( hdr )
        else:
            if isinstance( hdr, list ):
                oLines += hdr
            else:
                print >> sys.stderr, "Error: wrong hdr type for shellScript !"
                sys.exit(0)
    
    # -- Command(s)
    # if cmd is a list, let's unlist it !
    if isinstance( cmd, str ):
        oLines.append( cmd )
    else:
        if isinstance( cmd, list ):
            oLines += cmd
        else:
            print >> sys.stderr, "Error: wrong cmd type for shellScript !"
            sys.exit(0)
            
    filePutLines( outFile, oLines, ret = 1 )

    # Make this script executable
    os.chmod( outFile, 0755 )

    if verbose:
        print >> sys.stderr, "ShellScript '%s' successfully created" % outFile

    return

def dateLimConv( dateLim ):
    """
    Convert input user limit dates with the format:
    YY_MM_DD::YY_MM_DD
    to unix timestamp

    @param dateLim: user limit date
    """
    
    if dateLim == None:
        return (None,None)

    sp = dateLim.split("::")

    if not len(sp[0]):
        s = None
    else:
        s = time.mktime(time.strptime(sp[0], "%y_%m_%d"))
    if not len(sp[1]):
        e = None
    else:
        e = time.mktime(time.strptime(sp[1], "%y_%m_%d"))

    return (s,e)

def blacklistConv( blacklist ):
    """
    Convert input user ip mask with the format
    192.168.*.*::172.27.6.*
    to simple list

    @param blacklist: ip masks
    """
    
    if blacklist == None:
        return []

    sp = blacklist.split("::")
    ip = []
    for aIP in sp:
        ip.append( aIP.split(".") )
        
    return ip

def serviceConv( service ):
    """
    Convert user input services with the format
    service1::service2::service3
    
    @param service: services definition
    """
    if service == None:
        return None
    return service.split("::")

def isDateInLim( date, dateLim ):
    """
    Check if provided date is in the dateLim interval

    @param date: query date
    @param dateLim: query date range (could be (None,None) )
    """

    if dateLim[0] == None and dateLim[1] == None:
        return True
    elif dateLim[0] == None:
        if date < dateLim[1]:
            return True
    elif dateLim[1] == None:
        if date > dateLim[0]:
            return True
    else:
        if date > dateLim[0] and date < dateLim[1]:
            return True
    return False

def isIPBlackListed( ip, blacklist ):
    """
    Check if the input ip is in the blacklist
    NB: works only with IPv4
    BlackList could contain * jokers    

    @param ip: query ip
    @param blacklist: ip list 
    """

    if isinstance(ip,str):
        ip = ip.split(".")

    for aBIP in blacklist:

        # Simple comparison
        if aBIP == ip:
            return True

        # Check for IP range
        if "*" in aBIP:
            sc = 0
            for i in range(0,4,1):
                if aBIP[i] == "*":
                    sc += 1
                    continue
                if ip[i] == aBIP[i]:
                    sc += 1
            if sc == 4:
                return True

    return False

def ip_geoloc( ip, data = "/usr/share/GeoIP/GeoLiteCity.dat" ):
    """
    Get IP geolocalisation with GeoIP
    Returns a tuple (country,city)
    """
    
    import GeoIP
    
    gi = GeoIP.open( data, GeoIP.GEOIP_STANDARD )  
    gir = gi.record_by_addr( ip )
    if gir is not None:
        country = gir[ 'country_name' ]
        city = gir['city']
        return country,city
    else:
        return None,None

def ip_geoloc2( ip, data = "/usr/share/GeoIP/GeoLite2-City_20210128/GeoLite2-City.mmdb" ):
    """
    Get IP geolocalisation with GeoLite2 data
    Returns a tuple (country,city)
    """

    from geoip import open_database
    import pycountry

    gi = open_database( data )

    gir = gi.lookup( ip )
    if gir is not None:
        country = gir.country
        country = pycountry.countries.get(alpha_2=country)
        if country is not None:
            country = country.name
        return country,None
    else:
        return None,None


class MobLog:
    """
    MobLog - Mobyle Log
    
    @author: Julien Maupetit

    @organisation : Ressource Parisienne en Bioinformatique Structurale
    (RPBS) - Universite Paris Diderot Paris 7 - MTi INSERM U973
    
    @contact: julien.maupetit@univ-paris-diderot.fr
    
    @version: %s
    """ % VERSION
    
    def __init__( self, fName, service = None, dateLim = None, blacklist = None, ipv = False, offMobLog = None, verbose = 0, debug = 0 ):
        """
        Initialize MobLog instance.

        @param 
        """

        self.debug = debug
        self.verbose = verbose
        self.ipv = ipv
        self.offMobLog = offMobLog
        
        self.fName = fName
        self.service = serviceConv( service )
        self.dateLim = dateLimConv( dateLim )
        self.blacklist = blacklistConv( blacklist )
        
        self.data  = {}
        self.ignored = []
        if self.offMobLog:
            self.offMobLogParse()
        if self.fName:
            self.mobLogParse()

        if not len(self.data.keys()):
            sys.exit("Nothing to do. See help (-h).")
        
        self.services = self.data.keys()
        self.services.sort()


    def __len__( self ):
        c=0
        for aSer in self.services:
            c += len( self.data[ aSer ] )
        return c

    def __repr__(self):
        """
        MobLog objet representation
        """
        oStr = ""
        for aSer in self.services:
            for aJob in self.data[ aSer ]:
                oStr += "%-20s %s %s %s %s\n" % (
                    aSer,
                    time.strftime("%Y/%m/%d" , time.localtime(aJob['date'])),
                    str(aJob['jobKey']),
                    str(aJob['email']),
                    str(aJob['ip'])
                    )
        return oStr

    def keyLenSort(self, a, b ):
        """
        Sort a list according to its length
        """

        if len(self.data[ a ]) > len(self.data[ b ]):
            return -1
        else:
            return 1

        return 0


    def mobLogParse( self ):
        """
        Parse the input log file. Format is:

        Mon, 26 May 2008 11:36:53 ADME-Tox eMail T04871813344002 192.168.1.171
        This will fill self.data list, which contains:
        [ date, service, email, jobKey, ip ]
        """

        for aFile in self.fName:
            lines = fileGetLines( aFile )

            for line in lines:

                if self.debug:
                    print >> sys.stderr, line
                    sys.stderr.flush()

                sp = line.split()

                # Date
                dateStr = line[:25]
                try:            
                    date = time.mktime(time.strptime(dateStr, "%a, %d %b %Y %H:%M:%S"))
                except ValueError:
                    print sys.stderr, "Warning: wrong time format for line:\n%s" % line
                    sys.exit(0)

                # Check date limits
                if not isDateInLim( date, self.dateLim ):
                    self.ignored.append( line )
                    continue

                # Service, Email, JobKey, IP
                if len(sp) == 10:
                    service, email, jobKey, ip, portal = sp[5:]

                elif len(sp) == 9:
                    service, email, ip, portal = sp[5:]

                else:
                    print >> sys.stderr, "Warning: wrong format for line:\n%s" % line
                    continue


                # Check only a particular service
                if self.service and service not in self.service:
                    self.ignored.append( line )
                    continue

                # Check blacklisted IP
                if isIPBlackListed( ip, self.blacklist ):
                    self.ignored.append( line )
                    continue

                if not self.data.has_key( service ):                
                    self.data[ service ] = []

                aJob = {}
                aJob['date']   = date
                aJob['email']  = email
                aJob['jobKey'] = jobKey
                aJob['ip']     = ip

                self.data[ service ].append( aJob )

            
    def offMobLogParse( self ):
        """
        Parse the input log file. Format is:

        2009-04-08:13.44 access from : 155.41.214.38  agent : 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)'
        This will fill self.data list, which contains:
        [ date, service, email, jobKey, ip ]
        """

        lines = fileGetLines( self.offMobLog )
        
        for i,line in enumerate(lines):

            if self.debug:
                print >> sys.stderr, line
                sys.stderr.flush()

            sp = line.split()

            if len(sp) < 8:
                print >> sys.stderr, "Wrong format for line %d (%s) Ignored." % (i+1,self.offMobLog)
                continue
            
            # Date
            dateStr = sp[0]
            date = time.mktime(time.strptime(dateStr, "%Y-%m-%d:%H.%M"))
            
            # Check date limits
            if not isDateInLim( date, self.dateLim ):
                continue

            # Service
            service = os.path.basename(self.offMobLog)
            
            # IP
            ip = sp[4]

            # Check blacklisted IP
            if isIPBlackListed( ip, self.blacklist ):
                self.ignored.append( line )
                continue

            if not self.data.has_key( service ):                
                self.data[ service ] = []

            aJob = {}
            aJob['date']   = date
            aJob['email']  = "john@doe.com"
            aJob['jobKey'] = "unkwn"
            aJob['ip']     = ip
            
            self.data[ service ].append( aJob )
            

    def usageHeader( self, stream = sys.stdout ):
        """
        Print report header
        """
        stream.write( "# -- \n" )
        stream.write( "# AccessLogInfos Report v%s\n"%VERSION )
        stream.write( "# -- \n" )
        if self.fName:
            stream.write( "# Mobyle  Log file: %s\n" % self.fName )
        if self.offMobLog:
            stream.write( "# Off Mob Log file: %s\n" % self.offMobLog )
        stream.write( "# Period %s-%s\n" % (
            time.strftime("%Y/%m/%d" , time.localtime(self.dateLim[0])),
            time.strftime("%Y/%m/%d" , time.localtime(self.dateLim[1])),
            )
                    )
    

    def sUsageReport( self ):
        """
        Service usage report
        """

        self.usageHeader()
        
        uIP = 0

        services = self.services
        services.sort( self.keyLenSort )

        # javascript report
        others = 0
        for idx, aSer in enumerate(services):
            if idx < 20:
                print >> sys.stdout, "['%s', %d]," % (aSer, len(self.data[ aSer ]))
            else:
                others += len(self.data[ aSer ])
        print >> sys.stdout, "['Others', %d]" % others

        for aSer in services:
            if self.ipv:
                print >> sys.stdout, "-" * 70

            print >> sys.stdout, "%-20s: %4d jobs %6.2f%%" % ( 
                aSer, 
                len(self.data[ aSer ]),
                len(self.data[ aSer ]) / float(len(self)) * 100.
                ),

            # get uniq ip
            aSerIps = {}
            for aJob in self.data[ aSer ]:
                if aJob['ip'] not in aSerIps.keys():
                    aSerIps[ aJob['ip'] ] = 0
                aSerIps[ aJob['ip'] ] += 1
            print >> sys.stdout, "%4d unique IPs" % len(aSerIps)
            uIP += len(aSerIps)

            if self.ipv:
                print >> sys.stdout, "-" * 70
                
                sIP = aSerIps.keys()
                sIP.sort()
                for aIP in sIP:
                    country,city = ip_geoloc2( aIP )                    
                    try:
                        dns = socket.gethostbyaddr(aIP)[0]
                    except:
                        dns = "unknown_host"
                    print >> sys.stdout, "%15s (%5d): %s - %s [%s]" % (aIP,aSerIps[aIP],str(country),str(city),dns)

        print >> sys.stdout, "# " + "-" * 68
        print >> sys.stdout, "%-20s: %4d jobs (%d unique IPs)" % ( "Total", len(self), uIP )

    def cUsageReport( self ):
        """
        Per country usage report
        """

        self.usageHeader()

        uIP = 0

        # We simply sum all jobs by country
        countries = {}

        services = self.services
        services.sort( self.keyLenSort )

        for aSer in services:

            # get uniq ip
            aSerIps = {}
            for aJob in self.data[ aSer ]:
                if aJob['ip'] not in aSerIps.keys():
                    aSerIps[ aJob['ip'] ] = 0
                aSerIps[ aJob['ip'] ] += 1
            uIP += len(aSerIps)
                
            sIP = aSerIps.keys()
            sIP.sort()

            # sum
            for aIP in sIP:
                country,city = ip_geoloc2( aIP )                    
                if countries.has_key( country ):
                    countries[ country ] += aSerIps[aIP]
                else:
                    countries[ country ]  = aSerIps[aIP]         

        c = sorted( countries.items(), key=itemgetter(1) )

        # Report
        for aC in c:
            print >> sys.stdout, "%20s : %-8d" % ( aC[0], aC[1] )
        
        # Javascript report
        for aC in sorted( countries.items(), key=itemgetter(1), reverse = True):
            print >> sys.stdout, "['%s', %.16f]," % ( aC[0], float(aC[1]) / sum(aIP[1] for aIP in c))


    def mUsageReport( self ):
        """
        Per month usage report
        """

        self.usageHeader()
        
        uIP = 0

        # We simply sum all jobs by country
        countries = {}

        services = self.services
        services.sort( self.keyLenSort )
        
        # Jobs per Month
        jpm = {}

        for aSer in services:

            # get dates
            for aJob in self.data[ aSer ]:
                aKey = time.strftime("%Y%m" , time.localtime(aJob['date']))
                if jpm.has_key( aKey ):
                    jpm[ aKey ] += 1
                else:
                    jpm[ aKey ]  = 1

        m = sorted( jpm.items() )

        # Get LOCAL month
        months = []
        for i in range(1,13):
            months.append( datetime.date(2009, i, 1).strftime('%B') )

        # Report
        for aM in m:
            print >> sys.stdout, "%4d %-10s : %-8d" % ( int(aM[0][:-2]), months[ int(aM[0][-2:]) -1 ], aM[1] )


# -- COMMAND LINE --
def process( args, options ):
    """
    Process to command line imposed analyse.
    """    

    if len(args):
        fName = args
    else:
        fName = None

    mob = MobLog( fName, service = options.service, dateLim = options.dateLim,
                  blacklist = options.blacklist, ipv = options.ipv,
                  offMobLog = options.offMobLog,
                  verbose = options.verbose, debug = options.debug )
    
    if options.cUsage:
        mob.cUsageReport()
    elif options.mUsage:
        mob.mUsageReport()
    else:
        mob.sUsageReport()

    return


def main( argv ):
    """
    Parse the command line and launch the main purpose.
    """

    (options, args) = argParse( )
    ret = process( args, options )

    return ret


# GO !
if __name__ == "__main__":
    main(sys.argv)

